package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.navigation.fragment.NavHostFragment;

public class VueListe extends Fragment implements Observer<Integer>/* TODO Q7 */ {

    private SuiviViewModel suiviViewModel;
    // TODO Q6
    
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_liste, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        suiviViewModel = new SuiviViewModel(getActivity().getApplication());
        LiveData<Integer> liveNextQuestion = suiviViewModel.getLiveNextQuestion();
        liveNextQuestion.observe(getViewLifecycleOwner(), this);
        suiviViewModel.setNextQuestion(liveNextQuestion.getValue());

        view.findViewById(R.id.btnToGenerale).setOnClickListener(view1 ->
                NavHostFragment.findNavController(VueListe.this).navigate(R.id.liste_to_generale));

        suiviViewModel.observeSelection(this, this);
        suiviViewModel.observe(this, this);

        // TODO Q6.b
        // TODO Q7
        // TODO Q8
    }

    @Override
    public void onChanged(Integer integer) {
        suiviViewModel.setNextQuestion(integer);
    }
}