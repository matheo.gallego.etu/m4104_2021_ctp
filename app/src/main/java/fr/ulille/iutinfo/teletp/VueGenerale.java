package fr.ulille.iutinfo.teletp;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment implements Observer<Integer> {

    private String salle;
    private String poste;
    private String DISTANCIEL;
    private SuiviViewModel suiviViewModel;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        this.poste = "";
        this.salle = DISTANCIEL;

        suiviViewModel = new SuiviViewModel(getActivity().getApplication());
        LiveData<Integer> liveNextQuestion = suiviViewModel.getLiveNextQuestion();
        liveNextQuestion.observe(getViewLifecycleOwner(), this);
        suiviViewModel.setNextQuestion(liveNextQuestion.getValue());

        Spinner spnSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> adapterSalle = ArrayAdapter.createFromResource(getContext(),
                R.array.list_salles, android.R.layout.simple_spinner_item);
        adapterSalle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSalle.setAdapter(adapterSalle);

        Spinner spnPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> adapterPoste = ArrayAdapter.createFromResource(getContext(),
                R.array.list_postes, android.R.layout.simple_spinner_item);
        adapterPoste.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnPoste.setAdapter(adapterPoste);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            Toast login = Toast.makeText(getActivity(), getActivity().findViewById(R.id.tvLogin).toString(), Toast.LENGTH_LONG);
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        suiviViewModel.observeSelection(this, this);
        suiviViewModel.observe(this, this);

        update();
        // TODO Q9
    }

    @Override
    public void onChanged(Integer integer) {
        suiviViewModel.setNextQuestion(integer);
        update();
    }

    public void update() {
        Spinner spnPoste = getActivity().findViewById(R.id.spPoste);
        if (salle == DISTANCIEL) {
            spnPoste.setVisibility(View.INVISIBLE);
            spnPoste.findViewById(R.id.spPoste).setEnabled(false);
        } else {
            spnPoste.setVisibility(View.VISIBLE);
            spnPoste.findViewById(R.id.spPoste).setEnabled(true);
        }
        suiviViewModel.setLocalisation("Distanciel");
    }
    // TODO Q9
}