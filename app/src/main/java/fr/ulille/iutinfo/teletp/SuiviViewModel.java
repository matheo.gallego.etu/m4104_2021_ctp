package fr.ulille.iutinfo.teletp;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

public class SuiviViewModel extends AndroidViewModel {

    private MutableLiveData<String> liveLocalisation;
    private MutableLiveData<String> liveUsername;
    private MutableLiveData<Integer> liveNextQuestion;
    private String[] questions;

    public SuiviViewModel(Application application) {
        super(application);
        liveLocalisation = new MutableLiveData<>();
        liveUsername = new MutableLiveData<>();
        liveNextQuestion = new MutableLiveData<>(0);
    }

    public LiveData<String> getLiveUsername() {
        return liveUsername;
    }

    public void setUsername(String username) {
        liveUsername.setValue(username);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Username : " + username, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getUsername() {
        return liveUsername.getValue();
    }
    public LiveData<String> getLiveLocalisation() {
        return liveLocalisation;
    }

    public void setLocalisation(String localisation) {
        liveLocalisation.setValue(localisation);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Localisation : " + localisation, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getLocalisation() {
        return liveLocalisation.getValue();
    }

    //initialise la liste des questions depuis la ressource
    void initQuestions(Context context) {
        this.questions = context.getResources().getStringArray(R.array.list_questions);
    }

    //retourne la quesion d’indice position (entre 0 et le nombre de questions-1).
    String getQuestions(int position) {
        return questions[position];
    }

    //retourne l’objet LiveData pour nextQuestion
    LiveData<Integer> getLiveNextQuestion() {
        return liveNextQuestion;
    }

    //setter (pour la donnée associée au LiveData)
    void setNextQuestion(Integer nextQuestion) {
        this.liveNextQuestion.setValue(nextQuestion);
    }

    //getter (pour la donnée associée au LiveData)
    Integer getNextQuestion() {
        return liveNextQuestion.getValue();
    }

    @MainThread
    public void observe(@NonNull LifecycleOwner owner, @NonNull Observer<? super Integer> observer) {
        liveNextQuestion.observe(owner, observer);
    }

    @MainThread
    public void observeSelection(@NonNull LifecycleOwner owner, @NonNull Observer<? super Integer> observer) {
        getLiveNextQuestion().observe(owner, observer);
    }

}
